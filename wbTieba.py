# -*- coding: utf8 -*-
'''
The wbTieba module.
Author: wbTieba Team
Data: 2014-2-19
Version: 0.01.06
'''

import urllib
import urllib2
import cookielib
import socket
import simplejson
import re
import time
import random
from utilities import *

class webUser(object):
    username = ''
    password = ''
    def __init__(self, username, password):
        self.username = username
        self.password = password

class wbTieba(webUser):
    def __init__(self, username, password):
        '''
        生成一个 wbTieba 类的实例
        包括 username 和 password
        通过 cookielib 库操作 cookies
        '''
        # 记录 username 和 password
        webUser.__init__(self, username, password)
        # 自动处理 cookie, 能处理 HTTP response 中的 Set-Cookie
        # 构建一个 CookieJar 类型的实例
        self.cj    = cookielib.CookieJar()
        # build_opener 方法会构造一个 OpenerDirector 类的实例
        # OpenerDirector 类的实例下辖许多 Handle 类
        # 此处的 HTTPCookieProcessor 就是一例
        self.opener= urllib2.build_opener(urllib2.HTTPCookieProcessor(self.cj))
        # install_opener 方法的效果是用我们的 OpenerDirector 实例
        # 替换掉 urllib2 默认的 OpenerDirector
        # urllib2.install_opener(self.opener)

    def urlopen(self, url, data = None, timeout=socket._GLOBAL_DEFAULT_TIMEOUT):
        '''
        A new urlopen method that will not conflict with the original one.
        '''
        return self.opener.open(url, data, timeout)

    def is_login(self):
        '''
        baiduC.Tbs 中有 is_login 字段，可以用来判断登录状态
        '''
        return simplejson.loads(self.urlopen(baiduC.Tbs).read())["is_login"]

    def get_common_tbs(self):
        '''
        get common tbs data
        '''
        return simplejson.loads(self.urlopen(baiduC.Tbs).read())["tbs"]

    def login(self, vf_code = ''):
        '''
        login
        '''
        self.urlopen(baiduC.Index)
        print "%s is logging in ..." % self.username
        self.token       = self.urlopen(baiduC.Token).read()
        self.matchVal    = re.search(u'"token" : "(?P<tokenVal>.*?)"', self.token)
        self.tokenVal    = self.matchVal.group('tokenVal')

        self.postData    = {
            'username'   : self.username,
            'password'   : self.password,
            'u'          : 'https://passport.baidu.com/',
            'tpl'        : 'pp',
            'token'      : self.tokenVal,
            'staticpage' : 'https://passport.baidu.com/static/passpc-account/html/v3Jump.html',
            # 'verifycode' : vf_code,
            # 'isPhone'    : 'false',
            # 'charset'    : 'UTF-8',
            # 'callback'   : 'parent.bd__pcbs__ra48vi'
            }
        self.postData = urllib.urlencode(self.postData)

        self.loginRequest = urllib2.Request(baiduC.WebLogin, self.postData)
        self.loginRequest.add_header('Accept','text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')
        self.loginRequest.add_header('Accept-Encoding','gzip,deflate,sdch')
        self.loginRequest.add_header('Accept-Language','zh-CN,zh;q=0.8')
        self.loginRequest.add_header('User-Agent','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36')
        self.loginRequest.add_header('Content-Type','application/x-www-form-urlencoded')
        sendPost = self.urlopen(self.loginRequest)
        return self.is_login()

    def new_thread(self, kw, title, content, vcode = None):
        print 'Posting "%s" at "%s", please wating ...' % (title, kw)

        tempPWD        = get_mouse_pwd()
        self.changeRow = re.compile('\n')
        self.content   = re.sub(self.changeRow, '<br/>', content)

        self.postData = {
            'ie'          : 'utf-8',
            'kw'          : kw,      # 贴吧名字
            'fid'         : get_forum_id(kw),      # forum ID
            'tid'         : '0',     # post ID
            'floor_num'   : '0',     # 回帖楼层
            'rich_text'   : '1',     # 未知
            'tbs'         : self.get_common_tbs(),     # common tbs
            'content'     : self.content, # post content
            'title'       : title,   # post title
            'prefix'      : '',      # seem to be the prefix of the title?
            'files'       : '[]',    # seem to be the uploading files?
            'mouse_pwd'   : tempPWD[0],
            'mouse_pwd_t' : tempPWD[1],
            'mouse_pwd_isclick' : '0',
            '__type__'    : 'thread'
            # 'lp_type' : '0',
            # 'lp_sub_type' : '0',
            # 'anonymous' : '0'
        }

        if not vcode == None:
            self.postData.update({
                'vcode_md5' : '',      # verify code
                })
        self.postData  = urllib.urlencode(self.postData)
        newThread = urllib2.Request(baiduC.Thread, self.postData)
        newThread.add_header('Accept','application/json, text/javascript, */*; q=0.01')
        newThread.add_header('Accept-Encoding','gzip,deflate,sdch')
        newThread.add_header('Accept-Language','zh-CN,zh;q=0.8')
        newThread.add_header('User-Agent','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36')
        newThread.add_header('Content-Type','application/x-www-form-urlencoded')
        newThread.add_header('X-Requested-With','X-Requested-With: XMLHttpRequest')

        sendPost = self.urlopen(newThread)
        return None

    def new_post(self, kw, tid, content):
        print 'Posting "%s" at "%s", please wating ...' % (content, tid)

        tempPWD        = get_mouse_pwd()
        self.changeRow = re.compile('\n')
        self.content = re.sub(self.changeRow, '<br/>', content)

        self.postData = {
            'ie'          : 'utf-8',
            'kw'          : kw,
            'fid'         : get_forum_id(kw),
            'tid'         : tid,
            'vcode_md5'   : '',
            'floor_num'   : '',
            'rich_text'   : '1',
            'tbs'         : self.get_common_tbs(),
            'content'     : self.content,
            'files'       : '[]',
            'mouse_pwd'   : tempPWD[0],
            'mouse_pwd_t' : tempPWD[1]
        }

        self.postData  = urllib.urlencode(self.postData)
        newPost = urllib2.Request(baiduC.Post, self.postData)
        newPost.add_header('Accept','application/json, text/javascript, */*; q=0.01')
        newPost.add_header('X-Requested-With','X-Requested-With: XMLHttpRequest')
        newPost.add_header('User-Agent','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36')
        newPost.add_header('Content-Type','application/x-www-form-urlencoded')
        newPost.add_header('Accept-Encoding','gzip,deflate,sdch')
        newPost.add_header('Accept-Language','zh-CN,zh;q=0.8')

        sendPost = self.urlopen(newPost)
        return None

    def add_post(self, kw, tid, quote_id, content):
        print "Add a Posting %s in %s at %s ..." % (content, post, title)

        self.change_row = re.compile('\n')
        self.content = re.sub(self.change_row, '<br/>', content)

        self.post_data = {
            'ie'          : 'utf-8',
            'kw'          : kw,
            'fid'         : get_forum_id(kw),
            'tid'         : tid,
            'floor_num'   : '',
            'quote_id'    : quote_id,
            'rich_text'   : '1',
            'tbs'         : self.get_common_tbs(),
            'content'     : self.content,
            'lp_type'     : '0',
            'lp_sub_type' : ''
        }

        self.post_data  = urllib.urlencode(self.post_data)
        add_post = urllib2.Request(baiduC.Post, self.post_data)
        add_post.add_header('Accept','*/*')
        add_post.add_header('X-Requested-With','X-Requested-With: XMLHttpRequest')
        add_post.add_header('User-Agent','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36')
        add_post.add_header('Content-Type','application/x-www-form-urlencoded; charset=UTF-8')
        add_post.add_header('Accept-Encoding','gzip,deflate,sdch')
        add_post.add_header('Accept-Language','zh-CN,zh;q=0.8')

        sendPost = self.urlopen(add_post)
        print "Done."
        return None

    def add_top(self, kw, tid):
        self.postData = {
            'ie'  : 'utf-8',
            'tbs' : self.get_common_tbs(),
            'kw'  : kw,
            'fid' : get_forum_id(kw),
            'tid' : tid
        }
        self.postData  = urllib.urlencode(self.postData)

        addTop = urllib2.Request(baiduC.AddTop, self.postData)
        addTop.add_header('Accept','application/json, text/javascript, */*; q=0.01')
        addTop.add_header('Accept-Encoding','gzip,deflate,sdch')
        addTop.add_header('Accept-Language','zh-CN,zh;q=0.8')
        addTop.add_header('User-Agent','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36')
        addTop.add_header('Content-Type','application/x-www-form-urlencoded')
        addTop.add_header('X-Requested-With','X-Requested-With: XMLHttpRequest')

        sendPost = self.urlopen(addTop)

    def cancle_top(self, kw, tid):
        self.postData = {
            'ie'  : 'utf-8',
            'tbs' : self.get_common_tbs(),
            'kw'  : kw,
            'fid' : get_forum_id(kw),
            'tid' : tid
        }
        self.postData  = urllib.urlencode(self.postData)

        cancleTop = urllib2.Request(baiduC.CancleTop, self.postData)
        cancleTop.add_header('Accept','application/json, text/javascript, */*; q=0.01')
        cancleTop.add_header('Accept-Encoding','gzip,deflate,sdch')
        cancleTop.add_header('Accept-Language','zh-CN,zh;q=0.8')
        cancleTop.add_header('User-Agent','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36')
        cancleTop.add_header('Content-Type','application/x-www-form-urlencoded')
        cancleTop.add_header('X-Requested-With','X-Requested-With: XMLHttpRequest')

        sendPost = self.urlopen(cancleTop)

    def add_good(self, kw, tid, cid = 0):
        self.postData = {
            'ie'  : 'utf-8',
            'tbs' : self.get_common_tbs(),
            'kw'  : kw,
            'fid' : get_forum_id(kw),
            'tid' : tid,
            'cid' : cid
        }
        self.postData = urllib.urlencode(self.postData)

        AddGood       = urllib2.Request(baiduC.AddGood, self.postData)
        AddGood.add_header('Accept','application/json, text/javascript, */*; q=0.01')
        AddGood.add_header('Accept-Encoding','gzip,deflate,sdch')
        AddGood.add_header('Accept-Language','zh-CN,zh;q=0.8')
        AddGood.add_header('User-Agent','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36')
        AddGood.add_header('Content-Type','application/x-www-form-urlencoded')
        AddGood.add_header('X-Requested-With','X-Requested-With: XMLHttpRequest')

        sendPost = self.urlopen(AddGood)

    def cancle_good(self, kw, tid):
        self.postData = {
            'ie'  : 'utf-8',
            'tbs' : self.get_common_tbs(),
            'kw'  : kw,
            'fid' : get_forum_id(kw),
            'tid' : tid
        }
        self.postData = urllib.urlencode(self.postData)

        CancleGood    = urllib2.Request(baiduC.CancleGood, self.postData)
        CancleGood.add_header('Accept','application/json, text/javascript, */*; q=0.01')
        CancleGood.add_header('Accept-Encoding','gzip,deflate,sdch')
        CancleGood.add_header('Accept-Language','zh-CN,zh;q=0.8')
        CancleGood.add_header('User-Agent','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36')
        CancleGood.add_header('Content-Type','application/x-www-form-urlencoded')
        CancleGood.add_header('X-Requested-With','X-Requested-With: XMLHttpRequest')

        sendPost = self.urlopen(CancleGood)

    def sign(self, kw):
        print "sign at %s..." %(kw)

        self.post_data = {
            'ie'  : 'utf-8',
            'kw'  : kw,
            'tbs' : self.get_common_tbs()
        }

        self.post_data  = urllib.urlencode(self.post_data)
        sign = urllib2.Request(baiduC.Sign, self.post_data)
        sign.add_header('Accept','application/json, text/javascript, */*; q=0.01')
        sign.add_header('X-Requested-With','X-Requested-With')
        sign.add_header('User-Agent','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36')
        sign.add_header('Content-Type','application/x-www-form-urlencoded; charset=UTF-8')
        sign.add_header('Accept-Encoding','gzip,deflate,sdch')
        sign.add_header('Accept-Language','zh-CN,zh;q=0.8')

        sendPost = self.urlopen(sign)
        print "Done"

    def delete_thread(self, kw, tid):
        print 'Delete...'

        self.post_data = {
            'ie'          : 'utf-8',
            'tbs'         : self.get_common_tbs(),
            'kw'          : kw,
            'fid'         : get_forum_id(kw),
            'tid'         : tid,
            'user_name'   : self.username,
            'delete_my_post' : '0',
            'delete_my_thread' : '1',
        }

        self.post_data  = urllib.urlencode(self.post_data)
        deleteThread = urllib2.Request(baiduC.Post, self.postData)
        deleteThread.add_header('Accept','application/json, text/javascript, */*; q=0.01')
        deleteThread.add_header('X-Requested-With','XMLHttpRequest')
        deleteThread.add_header('User-Agent','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36')
        deleteThread.add_header('Content-Type','application/x-www-form-urlencoded')
        deleteThread.add_header('Accept-Encoding','gzip,deflate,sdch')
        deleteThread.add_header('Accept-Language','zh-CN,zh;q=0.8')

        sendPost = self.urlopen(deleteThread)
        print "完成"
        return None

if __name__ == '__main__':
    pass
