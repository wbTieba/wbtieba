# -*- coding: utf8 -*-
# file: utilities.py

import random
import time
import urllib2
import urllib
import re
import sys

currentEncoding = 'utf-8'

class baiduC(object):
    Index      = 'http://www.baidu.com/'
    WebLogin   = 'https://passport.baidu.com/v2/api/?login'
    LoginImg   = 'https://passport.baidu.com/?verifypic'
    Token      = 'https://passport.baidu.com/v2/api/?getapi&tpl=pp&apiver=v3&class=login'
    Post       = 'http://tieba.baidu.com/f/commit/post/add'
    Thread     = 'http://tieba.baidu.com/f/commit/thread/add'
    Delete     = 'http://tieba.baidu.com/f/commit/thread/delete'
    Tbs        = 'http://tieba.baidu.com/dc/common/tbs'
    Vcode      = 'http://tieba.baidu.com/f/user/json_vcode?lm=%s&rs10=2&rs1=0&t=0.7'
    Img        = 'http://tieba.baidu.com/cgi-bin/genimg?%s'
    AddTop     = 'http://tieba.baidu.com/f/commit/thread/top/add'
    CancleTop  = 'http://tieba.baidu.com/f/commit/thread/top/cancle'
    GoodClass  = 'http://tieba.baidu.com/f/center/goodclass?kw='
    AddGood    = 'http://tieba.baidu.com/f/commit/thread/good/add'
    CancleGood = 'http://tieba.baidu.com/f/commit/thread/good/cancle'
    Sign       = 'http://tieba.baidu.com/sign/add'

def get_timestamp():
    return int(time.time() * 1000)

def get_gmt8():
    return time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time.time() + 60 * 60 * 8))

def get_mouse_pwd():
    pwdRandom   = ','.join([str(random.randint(10, 120)) for x in range(44)])
    timeStamp   = get_timestamp()
    mouse_pwd   = pwdRandom + str(timeStamp) + '1'
    mouse_pwd_t = str(timeStamp)
    return [mouse_pwd, mouse_pwd_t]

def get_forum_id(kw):
    kwSource      = urllib2.urlopen('http://tieba.baidu.com/f?kw=%s' % kw).read()
    searchForumID = re.compile(r"{forumId : '(\d+)'}").search
    match         = searchForumID(kwSource)
    return match.group(1)

def get_tid(kw, title):
    title     = title.decode('utf-8').encode('gbk')
    kwSource  = urllib2.urlopen('http://tieba.baidu.com/f?ie=utf-8&kw=%s' % kw).read()#.decode('gbk').encode('utf-8')
    searchTID = re.compile(r'<a href="/p/(\d+)" title="' + title).search
    hit       = searchTID(kwSource)
    return hit.group(1)

def get_tid_title_author(kw, title, author):
    kwSource  = urllib2.urlopen('http://tieba.baidu.com/f?ie=utf-8&kw=%s' % kw).read().decode('gbk').encode('utf-8')
    searchTID = re.compile(r'<a href="/p/(\d+)" title="' + title + '.*?主题作者: ' + author).search
    hit       = searchTID(kwSource)
    return hit.group(1)

def get_quote_id(tid, floor_num):
    floor_num = floor_num
    source    = urllib2.urlopen('http://tieba.baidu.com/p/' + tid).read()
    hit       = re.search(r'&quot;pid&quot;:(\d+),&quot;floor_num&quot;:' + floor_num, source)
    return hit.group(1)


def get_good_classes(kw):
    kw              = urllib.quote(kw.decode(currentEncoding).encode('gbk'))
    goodClassSource = urllib2.urlopen(baiduC.GoodClass + kw).read()
    searchPairs     = re.findall(r'"class_id":(\d),"class_name":"(.*?)"', goodClassSource)
    return dict((int(x), y) for x, y in searchPairs)

if __name__ == '__main__':
    pass
